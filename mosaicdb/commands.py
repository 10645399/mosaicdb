#! /usr/bin/env/python
from __future__ import print_function


import os
import sys
import click
import subprocess


here = os.path.dirname(__file__)


@click.command()
@click.option('--host', default='localhost', help='Hostname of the MySql server. Default value is `localhost`')
@click.option('--port', default=3306, help='Port on which MySql server is running. Default value is `3306`')
@click.option('--database', default='maxiq', help='Database name. Default value is `maxiq`')
@click.option('--username', default='root', help='Username of the MySql user. Default value is `root`')
@click.option('--password', default='root', help='Password of the MySql user. Default value is `root`')
@click.pass_context
def install(ctx, host, port, database, username, password):
    """ Install schema and data for mosaic decisions """
    ctx.forward(install_schema)
    ctx.forward(install_data)


@click.command()
@click.option('--host', default='localhost', help='Hostname of the MySql server. Default value is `localhost`')
@click.option('--port', default=3306, help='Port on which MySql server is running. Default value is `3306`')
@click.option('--database', default='maxiq', help='Database name. Default value is `maxiq`')
@click.option('--username', default='root', help='Username of the MySql user. Default value is `root`')
@click.option('--password', default='root', help='Password of the MySql user. Default value is `root`')
def install_schema(host, port, database, username, password):
    """ Install schema for mosaic decisions  """
    print('Installing schema...')
    schema_file = open(os.path.join(here, 'sql', 'schema.sql'))
    p = subprocess.Popen([
        'mysql',
        '--host={}'.format(host),
        '--port={}'.format(port),
        '--database={}'.format(database),
        '--user={}'.format(username),
        '--password={}'.format(password),
        '--force'],
        stdin=schema_file,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    output, errors = p.communicate()
    print('--- STDOUT ---')
    print(output)
    print('--- STDERR ---')
    print(errors)


@click.command()
@click.option('--host', default='localhost', help='Hostname of the MySql server. Default value is `localhost`')
@click.option('--port', default=3306, help='Port on which MySql server is running. Default value is `3306`')
@click.option('--database', default='maxiq', help='Database name. Default value is `maxiq`')
@click.option('--username', default='root', help='Username of the MySql user. Default value is `root`')
@click.option('--password', default='root', help='Password of the MySql user. Default value is `root`')
def install_data(host, port, database, username, password):
    """ Install data for mosaic decisions """
    print('Installing data...')
    data_file = open(os.path.join(here, 'sql', 'data.sql'))
    p = subprocess.Popen([
        'mysql',
        '--host={}'.format(host),
        '--port={}'.format(port),
        '--database={}'.format(database),
        '--user={}'.format(username),
        '--password={}'.format(password),
        '--force'],
        stdin=data_file,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    output, errors = p.communicate()
    print('--- STDOUT ---')
    print(output)
    print('--- STDERR ---')
    print(errors)

